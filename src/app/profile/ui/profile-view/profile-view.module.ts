import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileViewComponent } from './profile-view.component';
import { MatIconModule } from '@angular/material/icon';
import { MainContentWrapperModule } from '../../../shared/main-content-wrapper/main-content-wrapper.module';

@NgModule({
  declarations: [ProfileViewComponent],
  imports: [CommonModule, MatIconModule, MainContentWrapperModule],
  exports: [ProfileViewComponent],
})
export class ProfileViewModule {}
