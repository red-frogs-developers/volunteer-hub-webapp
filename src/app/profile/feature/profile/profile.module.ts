import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfilePage } from './profile.page';
import { ProfileViewModule } from '../../ui/profile-view/profile-view.module';
import { MainContentWrapperModule } from '../../../shared/main-content-wrapper/main-content-wrapper.module';

@NgModule({
  declarations: [ProfilePage],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    ProfileViewModule,
    MainContentWrapperModule,
  ],
})
export class ProfileModule {}
