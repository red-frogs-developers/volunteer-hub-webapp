import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { ActivatedRoute, Params } from '@angular/router';
import { Observable, pipe, take } from 'rxjs';
import { User } from '../../../shared/models/user.model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  userId!: string;
  user?: Observable<User | undefined>;

  constructor(private route: ActivatedRoute, private angularFirestore: AngularFirestore) {}

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.userId = params['id'];
      this.user = this.angularFirestore.doc<User>('users/' + this.userId).valueChanges();
    })
  }
}
