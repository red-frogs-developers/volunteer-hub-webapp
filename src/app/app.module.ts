import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { QuillModule } from 'ngx-quill';

import {
  MAT_FORM_FIELD_DEFAULT_OPTIONS,
  MatFormFieldModule,
} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { AngularFireModule } from '@angular/fire/compat';
import { environment } from 'src/environments/environment';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { GroupNewPostDialogModule } from './group/ui/group-new-post-dialog/group-new-post-dialog.module';
import { NgxAuthFirebaseUIModule } from 'ngx-auth-firebaseui';
import { GroupAddMemberDialogComponent } from './group/ui/group-add-member-dialog/group-add-member-dialog.component';
import { MatListModule } from '@angular/material/list';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { MatCardModule } from '@angular/material/card';

@NgModule({
  declarations: [AppComponent, GroupAddMemberDialogComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatDividerModule,
    AngularFireModule.initializeApp(environment.firebase),
    NgxAuthFirebaseUIModule.forRoot(environment.firebase, () => '', {
      toastMessageOnAuthSuccess: false,
      passwordMinLength: 6,
      enableEmailVerification: false,
      authGuardLoggedInURL: '../',
    }),
    QuillModule.forRoot(),
    AngularFirestoreModule,
    MatProgressSpinnerModule,
    GroupNewPostDialogModule,
    MatListModule,
    MatCardModule,
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: { appearance: 'outline' },
    },
    {
      provide: MAT_DATE_LOCALE,
      useValue: 'nz',
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
