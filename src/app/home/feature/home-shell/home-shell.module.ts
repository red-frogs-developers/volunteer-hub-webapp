import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeShellRoutingModule } from './home-shell-routing.module';
import { HomeShellComponent } from './home-shell.component';
import { MainContentWrapperModule } from '../../../shared/main-content-wrapper/main-content-wrapper.module';

@NgModule({
  declarations: [HomeShellComponent],
  imports: [CommonModule, HomeShellRoutingModule, MainContentWrapperModule],
})
export class HomeShellModule {}
