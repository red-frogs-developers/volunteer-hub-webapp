import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import {
  UntypedFormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

@Component({
  selector: 'app-login-form',
  templateUrl: 'login-form.component.html',
})
export class LoginFormComponent implements OnInit {

  @Output()
  submit = new EventEmitter<{email: string, password: string}>();


  loginForm = this.formBuilder.group({
    email: ['', []],
    password: ['', []],
  });

  constructor(private formBuilder: UntypedFormBuilder) {}

  ngOnInit() {}

  onSubmit() {

    this.submit.emit({
      email: this.loginForm.value['email'],
      password: this.loginForm.value['password']
    })

    this.loginForm.reset();
    this.loginForm.markAsUntouched();
  }
}
