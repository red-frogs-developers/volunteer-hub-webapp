import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginShellRoutingModule } from './login-shell-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    LoginShellRoutingModule
  ]
})
export class LoginShellModule { }
