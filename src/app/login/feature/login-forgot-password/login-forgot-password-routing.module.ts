import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginForgotPasswordComponent } from './login-forgot-password.component';

const routes: Routes = [{ path: '', component: LoginForgotPasswordComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginForgotPasswordRoutingModule { }
