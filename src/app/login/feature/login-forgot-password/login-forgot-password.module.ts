import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginForgotPasswordRoutingModule } from './login-forgot-password-routing.module';
import { LoginForgotPasswordComponent } from './login-forgot-password.component';



@NgModule({
  declarations: [LoginForgotPasswordComponent],
  imports: [
    CommonModule,
    LoginForgotPasswordRoutingModule,
  ],
})
export class LoginForgotPasswordModule {}
