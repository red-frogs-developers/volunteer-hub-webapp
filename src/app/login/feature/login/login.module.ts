import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { LoginPage } from './login.page';
import { LoginFormModule } from '../../ui/login-form/login-form.module';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { ErrorPopupModule } from 'src/app/shared/error-popup/error-popup.module';
import { NgxAuthFirebaseUIModule } from 'ngx-auth-firebaseui';

@NgModule({
  declarations: [LoginPage],
  imports: [
    CommonModule,
    LoginRoutingModule,
    LoginFormModule,
    AngularFirestoreModule,
    ErrorPopupModule,
    NgxAuthFirebaseUIModule,
  ],
})
export class LoginModule {}
