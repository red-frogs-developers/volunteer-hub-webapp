import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Router } from '@angular/router';
import { CurrentUserService } from 'src/app/shared/services/current-user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  errorMessage: string = '';

  constructor(
    public auth: AngularFireAuth,
    private router: Router,
    private currentUserService: CurrentUserService
  ) {}

  ngOnInit(): void {}

  onSubmit(event: { email: string; password: string }) {
    this.errorMessage = '';
    this.auth
      .signInWithEmailAndPassword(event.email, event.password)
      .then((user) => {
        console.log('success');
        this.router.navigate(['../']);
      })
      .catch((error) => {
        console.error(error);
        this.errorMessage = 'Email or password is incorrect';
      });
  }

  onSuccess(){
    this.router.navigate(['../']);
  }
}
