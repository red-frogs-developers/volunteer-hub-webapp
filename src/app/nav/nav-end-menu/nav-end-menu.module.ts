import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavEndMenuComponent } from './nav-end-menu.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [NavEndMenuComponent],
  imports: [CommonModule, MatMenuModule, MatIconModule, MatButtonModule],
  exports: [NavEndMenuComponent],
})
export class NavEndMenuModule {}
