import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-nav-end-menu',
  templateUrl: './nav-end-menu.component.html',
  styleUrls: ['./nav-end-menu.component.scss'],
})
export class NavEndMenuComponent implements OnInit {
  @Input()
  imageUrl!: string;

  @Output()
  onProfile = new EventEmitter();

  @Output()
  onLogout = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  toProfile() {
    this.onProfile.emit();
  }

  logOut() {
    this.onLogout.emit();
  }
}
