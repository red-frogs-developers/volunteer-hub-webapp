import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Router } from '@angular/router';
import firebase from 'firebase/compat';
import { User } from 'src/app/shared/models/user.model';
import { CurrentUserService } from 'src/app/shared/services/current-user.service';

@Component({
  selector: 'app-nav-toolbar',
  templateUrl: './nav-toolbar.component.html',
  styleUrls: ['./nav-toolbar.component.scss'],
})
export class NavToolbarComponent implements OnInit {
  @Output()
  toggle = new EventEmitter();

  user!: firebase.User | null;
  currentUser!: User | null;

  constructor(
    public auth: AngularFireAuth,
    private router: Router,
    public currentUserService: CurrentUserService
  ) {}

  ngOnInit(): void {
    this.auth.user.subscribe((user) => {
      this.user = user;
      this.currentUserService.getUserDetails(user?.uid);
      this.currentUserService.currentUser.subscribe((currentUser) => {
        this.currentUser = currentUser;
        if (this.currentUser && !this.currentUser?.roles) {
          this.currentUser.roles = {
              admin: false,
              staff: false,
              volunteer: false
          }
        }
      });
    });
  }

  toggleDrawer() {
    this.toggle.emit();
  }

  logOut() {
    this.auth.signOut().then(() => {
      this.router.navigate(['/login']);
    });
  }

  toProfile() {
    this.router.navigate(['/profile', this.user?.uid]);
  }
}
