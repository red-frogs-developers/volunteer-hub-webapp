import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavToolbarComponent } from './nav-toolbar.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { NavEndMenuModule } from '../nav-end-menu/nav-end-menu.module';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [NavToolbarComponent],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    NavEndMenuModule,
    RouterModule,
  ],
  exports: [NavToolbarComponent],
})
export class NavToolbarModule {}
