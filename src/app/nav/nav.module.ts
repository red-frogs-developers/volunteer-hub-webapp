import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NavRoutingModule } from './nav-routing.module';
import { NavComponent } from './nav.component';
import { NavDrawerModule } from './nav-drawer/nav-drawer.module';



@NgModule({
  declarations: [NavComponent],
  imports: [CommonModule, NavRoutingModule, NavDrawerModule],
  exports: [NavComponent],
})
export class NavModule {}
