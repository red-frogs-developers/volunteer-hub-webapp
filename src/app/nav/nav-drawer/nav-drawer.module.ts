import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavDrawerComponent } from './nav-drawer.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { NavToolbarModule } from '../nav-toolbar/nav-toolbar.module';




@NgModule({
  declarations: [NavDrawerComponent],
  imports: [CommonModule, MatSidenavModule, NavToolbarModule],
  exports: [NavDrawerComponent],
})
export class NavDrawerModule {}
