import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppShellRoutingModule } from './app-shell-routing.module';
import { AppShellComponent } from './app-shell.component';
import { NavModule } from '../nav/nav.module';


@NgModule({
  declarations: [
    AppShellComponent,
  ],
  imports: [
    CommonModule,
    AppShellRoutingModule,
    NavModule
  ]
})
export class AppShellModule { }
