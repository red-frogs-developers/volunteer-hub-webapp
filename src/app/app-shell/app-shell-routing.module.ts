import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppShellComponent } from './app-shell.component';

const routes: Routes = [
  {
    path: '',
    component: AppShellComponent,
    children: [
      {
        path: 'profile',
        loadChildren: () =>
          import('../profile/feature/profile-shell/profile-shell.module').then(
            (m) => m.ProfileShellModule
          ),
      },
      {
        path: 'groups',
        loadChildren: () =>
          import('../group/feature/group-shell/group-shell.module').then(
            (m) => m.GroupShellModule
          ),
      },
      {
        path: 'admin',
        loadChildren: () =>
          import('../admin/feature/admin-shell/admin-shell.module').then(
            (m) => m.AdminShellModule
          ),
      },

      {
        path: 'events',
        loadChildren: () =>
          import('../events/feature/events-shell/events-shell.module').then(
            (m) => m.EventsShellModule
          ),
      },
      {
        path: '',
        pathMatch: 'full',
        loadChildren: () =>
          import('../home/feature/home-shell/home-shell.module').then(
            (m) => m.HomeShellModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AppShellRoutingModule {}
