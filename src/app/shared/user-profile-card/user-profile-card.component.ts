import { Component, Input, OnInit } from '@angular/core';
import { UserDocument } from '../models/user.model';

@Component({
  selector: 'app-user-profile-card',
  templateUrl: './user-profile-card.component.html',
  styleUrls: ['./user-profile-card.component.scss'],
})
export class UserProfileCardComponent implements OnInit {
  @Input()
  user!: UserDocument;

  constructor() {}

  ngOnInit(): void {}
}
