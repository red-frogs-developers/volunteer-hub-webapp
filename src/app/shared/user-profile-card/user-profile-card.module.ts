import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserProfileCardComponent } from './user-profile-card.component';
import { MatListModule } from '@angular/material/list';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [UserProfileCardComponent],
  imports: [CommonModule, MatListModule, RouterModule],
  exports: [UserProfileCardComponent],
})
export class UserProfileCardModule {}
