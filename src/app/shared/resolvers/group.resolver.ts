import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router';
import { Observable } from 'rxjs';
import { GroupService } from '../services/group.service';
import { GroupDocument } from '../models/group.model';

@Injectable({
  providedIn: 'root',
})
export class GroupResolver implements Resolve<GroupDocument | undefined> {
  constructor(private groupService: GroupService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<GroupDocument | undefined> {
    let id = route.paramMap.get('id');
    if (!id) {
      id = '';
    }
    return this.groupService.getDocumentById(id);
  }
}
