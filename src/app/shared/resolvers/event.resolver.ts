import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router';
import { Observable } from 'rxjs';
import { EventDocument } from '../models/event.model';
import { EventService } from '../services/event.service';

@Injectable({
  providedIn: 'root',
})
export class EventResolver implements Resolve<EventDocument | undefined> {
  constructor(private eventService: EventService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<EventDocument | undefined> {
    let id = route.paramMap.get('id');
    if (!id) {
      id = '';
    }
    return this.eventService.getDocumentById(id);
  }
}
