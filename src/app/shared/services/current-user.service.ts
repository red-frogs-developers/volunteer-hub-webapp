import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { BehaviorSubject } from 'rxjs';
import { User, UserDocument } from '../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class CurrentUserService {
  currentUser = new BehaviorSubject<User | null>(null);

  constructor(private angularFirestore: AngularFirestore) {}

  getUserDetails(id?: string) {
    console.log(id);
    this.angularFirestore
      .doc<UserDocument>('users/' + id)
      .valueChanges()
      .subscribe((user) => {
        this.currentUser.next(user!);
      });
  }
}
