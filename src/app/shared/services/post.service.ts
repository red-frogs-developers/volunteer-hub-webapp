import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/compat/firestore';
import { CurrentUserService } from './current-user.service';
import { Post } from '../models/post.model';
import { first, Observable } from 'rxjs';
import { GroupService } from './group.service';
import { GroupDocument } from '../models/group.model';

@Injectable({
  providedIn: 'root',
})
export class PostService {
  private postsCollection: AngularFirestoreCollection<Post>;

  constructor(
    private angularFirestore: AngularFirestore,
    private currentUserService: CurrentUserService,
    private groupService: GroupService
  ) {
    this.postsCollection = angularFirestore.collection<Post>('posts');
  }

  createPostInGroup(content: string, group: GroupDocument) {
    this.currentUserService.currentUser
      .pipe(first())
      .subscribe((currentUser) => {
        if (currentUser) {
          const id = this.angularFirestore.createId();
          let post: Post = {
            author: currentUser.id,
            content: content,
            timestamp: Date.now(),
            id: id,
            comments: [],
            groupId: group.id,
          };
          this.postsCollection
            .doc(id)
            .set(post)
            .then(() => {
              this.groupService.addPost(id);
            });
        }
      });
  }

  getPostsIn(group: GroupDocument): Observable<Post[]> {
    return this.angularFirestore
      .collection<Post>('posts', (ref) =>
        ref.where('groupId', '==', group.id).orderBy('timestamp', 'desc')
      )
      .valueChanges();
  }
}
