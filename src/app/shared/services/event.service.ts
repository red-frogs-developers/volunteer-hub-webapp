import { Injectable } from '@angular/core';
import { EventDocument } from '../models/event.model';
import { AbstractFirestoreDocumentRepository } from '../../../../../angular-springfire';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { GroupService } from './group.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class EventService extends AbstractFirestoreDocumentRepository<EventDocument> {
  constructor(
    private angularFirestore: AngularFirestore,
    private groupService: GroupService
  ) {
    super('events', angularFirestore);
  }

  insertDocument(document: EventDocument): Promise<void> {
    console.log(document);
    return this.getCollectionReference()
      .doc(document.id)
      .set(
        Object.assign({
          id: document.id,
          group: document.group,
          name: document.name,
          description: document.description,
          eventLocation: document.eventLocation,
          meetupLocation: document.meetupLocation,
          startDate: document.startDate,
          startTime: document.startTime,
          endDate: document.endDate,
          endTime: document.endTime,
          organizer: document.organizer,
          responses: document.responses,
        })
      );
  }

  createEventInGroup(event: EventDocument) {
    this.insertDocument(event).then(() => {
      this.groupService.addEvent(event.id);
    });
  }

  getEventsInArray(input: string[]): Observable<EventDocument[]> {
    return this.angularFirestore
      .collection<EventDocument>('events', (ref) =>
        ref.where('id', 'in', input)
      )
      .valueChanges();
  }

  getEventsUserIsInvitedTo(id: string) {
    return this.angularFirestore
      .collection<EventDocument>('events', (ref) =>
        ref.where('responses.invited', 'array-contains', id)
      )
      .valueChanges();
  }
}
