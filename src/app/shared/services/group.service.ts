import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument,
} from '@angular/fire/compat/firestore';
import { GroupDocument } from '../models/group.model';
import { BehaviorSubject, first } from 'rxjs';
import { Post } from '../models/post.model';
import { AbstractFirestoreDocumentRepository } from '../../../../../angular-springfire';

@Injectable({
  providedIn: 'root',
})
export class GroupService extends AbstractFirestoreDocumentRepository<GroupDocument> {
  groups = new BehaviorSubject<GroupDocument[]>([]);
  currentGroup = new BehaviorSubject<GroupDocument | null>(null);
  currentGroupDoc: AngularFirestoreDocument<GroupDocument> | null = null;
  openGroup: GroupDocument | null = null;
  private postsCollection: AngularFirestoreCollection<Post>;

  constructor(private angularFirestore: AngularFirestore) {
    super('groups', angularFirestore);
    this.postsCollection = angularFirestore.collection<Post>('posts');
  }

  getGroups() {
    this.getCollection().subscribe((groups) => {
      this.groups.next(groups);
    });
  }

  getCurrentGroup(id: string) {
    this.getDocumentById(id).subscribe((group) => {
      this.currentGroup.next(group!);
      this.openGroup = group!;
    });
  }

  addPost(postId: string) {
    this.getActiveDocumentSubscription()
      .pipe(first())
      .subscribe((document) => {
        document?.posts.push(postId);
        this.updateActiveDocument(document);
      });
  }

  addEvent(eventId: string) {
    this.getActiveDocumentSubscription()
      .pipe(first())
      .subscribe((document) => {
        document?.events.push(eventId);
        this.updateActiveDocument(document);
      });
  }

  addMembers(users: string[]) {
    this.getActiveDocumentSubscription()
      .pipe(first())
      .subscribe((document) => {
        if (document) {
          document.members = document.members.concat(users);
          this.updateActiveDocument(document);
        }
      });
  }

  insertDocument(document: GroupDocument): Promise<void> {
    return this.getCollectionReference()
      .doc(document.id)
      .set(
        Object.assign({
          admins: document.admins,
          events: document.events,
          members: document.members,
          posts: document.posts,
          name: document.name,
          profile_picture: document.profile_picture,
          description: document.description,
          public: document.public,
          id: document.id,
        })
      );
  }

  getGroupsUserIsMember(id: string) {
    return this.angularFirestore
      .collection<GroupDocument>('groups', (ref) =>
        ref.where('members', 'array-contains', id)
      )
      .valueChanges();
  }
}
