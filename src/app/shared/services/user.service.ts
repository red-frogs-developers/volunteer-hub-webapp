import { Injectable } from '@angular/core';
import { User, UserDocument } from '../models/user.model';
import {
  AngularFirestore,
  AngularFirestoreDocument,
} from '@angular/fire/compat/firestore';
import { AbstractFirestoreDocumentRepository } from '../../../../../angular-springfire';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UserService extends AbstractFirestoreDocumentRepository<UserDocument> {
  constructor(private angularFirestore: AngularFirestore) {
    super('users', angularFirestore);
  }

  insertDocument(document: UserDocument): Promise<void> {
    return this.getCollectionReference()
      .doc(document.id)
      .set(
        Object.assign({
          active: document.active,
          email: document.email,
          id: document.id,
          name: document.name,
          phone_number: document.phone_number,
          profile_picture: document.profile_picture,
          region: document.region,
          roles: document.roles,
        })
      );
  }

  getUserDocById(id?: string): AngularFirestoreDocument<User> {
    return this.angularFirestore.doc<User>('users/' + id);
  }

  getUsersInArray(input: string[]): Observable<UserDocument[]> {
    return this.angularFirestore
      .collection<UserDocument>('users', (ref) => ref.where('id', 'in', input))
      .valueChanges();
  }

  getUsersNotInArray(input: string[]): Observable<UserDocument[]> {
    return this.angularFirestore
      .collection<UserDocument>('users', (ref) =>
        ref.where('id', 'not-in', input)
      )
      .valueChanges();
  }
}
