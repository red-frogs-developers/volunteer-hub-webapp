import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { EventDocument } from '../../models/event.model';

@Component({
  selector: 'app-event-list-view',
  templateUrl: './event-list-view.component.html',
  styleUrls: ['./event-list-view.component.scss'],
})
export class EventListViewComponent implements OnInit {
  @Input()
  event!: EventDocument;

  @Output()
  navigate: EventEmitter<EventDocument> = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  onNavigate() {
    this.navigate.emit(this.event);
  }
}
