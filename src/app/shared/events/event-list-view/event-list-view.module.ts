import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventListViewComponent } from './event-list-view.component';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [EventListViewComponent],
  imports: [
    CommonModule,
    MatCardModule,
    MatDividerModule,
    MatIconModule,
    MatButtonModule,
    RouterModule,
  ],
  exports: [EventListViewComponent],
})
export class EventListViewModule {}
