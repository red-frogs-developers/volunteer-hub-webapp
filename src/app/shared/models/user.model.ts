import { AbstractFirestoreDocument } from '../../../../../angular-springfire';

export interface User {
  active: boolean;
  email: string;
  id: string;
  name: string;
  phone_number: string;
  profile_picture: string;
  region: string;
  roles: {
    admin: boolean;
    staff: boolean;
    volunteer: boolean;
  };
}

export class UserDocument extends AbstractFirestoreDocument {
  constructor(
    private _active: boolean,
    private _email: string,
    private _name: string,
    private _phone_number: string,
    private _profile_picture: string,
    private _region: string,
    private _roles: {
      admin: boolean;
      staff: boolean;
      volunteer: boolean;
    },
    id: string
  ) {
    super(id, 'users');
  }
  get active(): boolean {
    return this._active;
  }

  set active(value: boolean) {
    this._active = value;
  }

  get email(): string {
    return this._email;
  }

  set email(value: string) {
    this._email = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get phone_number(): string {
    return this._phone_number;
  }

  set phone_number(value: string) {
    this._phone_number = value;
  }

  get profile_picture(): string {
    return this._profile_picture;
  }

  set profile_picture(value: string) {
    this._profile_picture = value;
  }

  get region(): string {
    return this._region;
  }

  set region(value: string) {
    this._region = value;
  }

  get roles(): { admin: boolean; staff: boolean; volunteer: boolean } {
    return this._roles;
  }

  set roles(value: { admin: boolean; staff: boolean; volunteer: boolean }) {
    this._roles = value;
  }
}
