export interface Post {
  author: string;
  content: string;
  timestamp: number;
  id: string;
  comments: string[];
  groupId: string;
}
