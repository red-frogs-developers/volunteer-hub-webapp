import { AbstractFirestoreDocument } from '../../../../../angular-springfire';

export class EventDocument extends AbstractFirestoreDocument {
  get group(): string {
    return this._group;
  }

  set group(value: string) {
    this._group = value;
  }
  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get description(): string {
    return this._description;
  }

  set description(value: string) {
    this._description = value;
  }

  get eventLocation(): string {
    return this._eventLocation;
  }

  set eventLocation(value: string) {
    this._eventLocation = value;
  }

  get meetupLocation(): string {
    return this._meetupLocation;
  }

  set meetupLocation(value: string) {
    this._meetupLocation = value;
  }

  get startDate(): number {
    return this._startDate;
  }

  set startDate(value: number) {
    this._startDate = value;
  }

  get startTime(): string {
    return this._startTime;
  }

  set startTime(value: string) {
    this._startTime = value;
  }

  get endDate(): number {
    return this._endDate;
  }

  set endDate(value: number) {
    this._endDate = value;
  }

  get endTime(): string {
    return this._endTime;
  }

  set endTime(value: string) {
    this._endTime = value;
  }

  get organizer(): string {
    return this._organizer;
  }

  set organizer(value: string) {
    this._organizer = value;
  }

  get responses(): {
    invited: string[];
    going: string[];
    notGoing: string[];
    tentative: string[];
    partial: string[];
  } {
    return this._responses;
  }

  set responses(value: {
    invited: string[];
    going: string[];
    notGoing: string[];
    tentative: string[];
    partial: string[];
  }) {
    this._responses = value;
  }
  constructor(
    id: string,
    private _name: string,
    private _group: string,
    private _description: string,
    private _eventLocation: string,
    private _meetupLocation: string,
    private _startDate: number,
    private _startTime: string,
    private _endDate: number,
    private _endTime: string,
    private _organizer: string,
    private _responses: {
      invited: string[];
      going: string[];
      notGoing: string[];
      tentative: string[];
      partial: string[];
    }
  ) {
    super(id, 'events');
  }
}
