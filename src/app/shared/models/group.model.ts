import { AbstractFirestoreDocument } from '../../../../../angular-springfire';

export class GroupDocument extends AbstractFirestoreDocument {
  get description(): string {
    return this._description;
  }

  set description(value: string) {
    this._description = value;
  }

  get public(): boolean {
    return this._public;
  }

  set public(value: boolean) {
    this._public = value;
  }
  get admins(): string[] {
    return this._admins;
  }

  set admins(value: string[]) {
    this._admins = value;
  }

  get events(): string[] {
    return this._events;
  }

  set events(value: string[]) {
    this._events = value;
  }

  get members(): string[] {
    return this._members;
  }

  set members(value: string[]) {
    this._members = value;
  }

  get posts(): string[] {
    return this._posts;
  }

  set posts(value: string[]) {
    this._posts = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get profile_picture(): string {
    return this._profile_picture;
  }

  set profile_picture(value: string) {
    this._profile_picture = value;
  }
  constructor(
    private _admins: string[],
    private _events: string[],
    private _members: string[],
    private _posts: string[],
    private _name: string,
    private _profile_picture: string,
    private _description: string,
    private _public: boolean,
    id: string
  ) {
    super(id, 'groups');
  }
}
