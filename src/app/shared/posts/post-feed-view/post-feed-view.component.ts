import { Component, Input, OnInit } from '@angular/core';
import { Post } from '../../models/post.model';
import { User } from '../../models/user.model';
import { UserService } from '../../services/user.service';
import { Observable } from 'rxjs';
import { AbstractLoadingComponent } from '../../AbstractLoadingComponent';

@Component({
  selector: 'app-post-feed-view',
  templateUrl: './post-feed-view.component.html',
  styleUrls: ['./post-feed-view.component.scss'],
})
export class PostFeedViewComponent
  extends AbstractLoadingComponent
  implements OnInit
{
  @Input()
  post!: Post;
  author!: User;

  constructor(public userService: UserService) {
    super();
  }

  ngOnInit(): void {
    if (this.post) {
      this.userService
        .getUserDocById(this.post.author)
        .valueChanges()
        .subscribe((user) => {
          if (user) {
            this.author = user;
            this.isLoading = false;
          }
        });
    }
  }
}
