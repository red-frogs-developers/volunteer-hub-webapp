import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostFeedViewComponent } from './post-feed-view.component';
import { QuillModule } from 'ngx-quill';
import { PostHeaderModule } from '../post-header/post-header.module';
import { MatDividerModule } from '@angular/material/divider';

@NgModule({
  declarations: [PostFeedViewComponent],
  imports: [CommonModule, QuillModule, PostHeaderModule, MatDividerModule],
  exports: [PostFeedViewComponent],
})
export class PostFeedViewModule {}
