import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostHeaderComponent } from './post-header.component';

@NgModule({
  declarations: [PostHeaderComponent],
  imports: [CommonModule],
  exports: [PostHeaderComponent],
})
export class PostHeaderModule {}
