import { Component, OnInit } from '@angular/core';
import { EventService } from '../../../shared/services/event.service';
import { EventDocument } from '../../../shared/models/event.model';
import { AbstractFirestoreSmartComponent } from '../../../../../../angular-springfire';
import { ActivatedRoute } from '@angular/router';
import { UserDocument } from '../../../shared/models/user.model';
import { UserService } from '../../../shared/services/user.service';

@Component({
  selector: 'app-event-page',
  templateUrl: './event-page.component.html',
  styleUrls: ['./event-page.component.scss'],
})
export class EventPageComponent
  extends AbstractFirestoreSmartComponent<EventDocument>
  implements OnInit
{
  invitedUsers: UserDocument[] = [];
  goingUsers: UserDocument[] = [];
  notGoingUsers: UserDocument[] = [];
  tentativeUsers: UserDocument[] = [];
  partialUsers: UserDocument[] = [];

  constructor(
    private eventService: EventService,
    private activatedRoute: ActivatedRoute,
    private userService: UserService
  ) {
    super(eventService);
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ event }) => {
      this.setupSubscriptionWithDocument(event);
      if (this.document) {
        this.userService
          .getUsersInArray(this.document.responses.invited)
          .subscribe((users) => {
            this.goingUsers = users.filter((user) => {
              return !!this.document?.responses.going.indexOf(user.id);
            });
            this.notGoingUsers = users.filter((user) => {
              return !!this.document?.responses.notGoing.indexOf(user.id);
            });
            this.tentativeUsers = users.filter((user) => {
              return !!this.document?.responses.tentative.indexOf(user.id);
            });
            this.partialUsers = users.filter((user) => {
              return !!this.document?.responses.partial.indexOf(user.id);
            });
            this.invitedUsers = users;
          });
      }
    });
  }
}
