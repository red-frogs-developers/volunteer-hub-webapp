import { Component, OnInit } from '@angular/core';
import { EventService } from '../../../shared/services/event.service';
import { CurrentUserService } from '../../../shared/services/current-user.service';
import { Observable, Subscription } from 'rxjs';
import { User } from '../../../shared/models/user.model';
import { EventDocument } from '../../../shared/models/event.model';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-events-list-page',
  templateUrl: './events-list-page.component.html',
  styleUrls: ['./events-list-page.component.scss'],
})
export class EventsListPageComponent implements OnInit {
  currentUserSubscription: Subscription | null = null;
  eventsSubscription: Subscription | null = null;
  events: Observable<EventDocument[]> | null = null;
  currentUser!: User | null;

  constructor(
    private eventService: EventService,
    private currentUserService: CurrentUserService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.currentUserSubscription =
      this.currentUserService.currentUser.subscribe((currentUser) => {
        if (currentUser) {
          this.currentUser = currentUser;
          this.events = this.eventService.getEventsUserIsInvitedTo(
            this.currentUser.id
          );
        }
      });
  }

  selectGroup(event: EventDocument) {
    this.router.navigate([event.id], { relativeTo: this.route }).then(() => {
      this.eventService.setActiveDocument(event);
    });
  }
}
