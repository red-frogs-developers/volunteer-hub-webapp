import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EventsShellRoutingModule } from './events-shell-routing.module';
import { EventPageComponent } from '../event-page/event-page.component';
import { MainContentWrapperModule } from '../../../shared/main-content-wrapper/main-content-wrapper.module';
import { EventsListPageComponent } from '../events-list-page/events-list-page.component';
import { EventListViewModule } from '../../../shared/events/event-list-view/event-list-view.module';
import { MatButtonModule } from '@angular/material/button';
import { MatTabsModule } from '@angular/material/tabs';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { UserProfileCardModule } from '../../../shared/user-profile-card/user-profile-card.module';
import { MatListModule } from '@angular/material/list';

@NgModule({
  declarations: [EventPageComponent, EventsListPageComponent],
  imports: [
    CommonModule,
    EventsShellRoutingModule,
    MainContentWrapperModule,
    EventListViewModule,
    MatButtonModule,
    MatTabsModule,
    MatIconModule,
    MatDividerModule,
    UserProfileCardModule,
    MatListModule,
  ],
})
export class EventsShellModule {}
