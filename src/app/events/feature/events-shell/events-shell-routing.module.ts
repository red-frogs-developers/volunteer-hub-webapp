import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EventPageComponent } from '../event-page/event-page.component';
import { EventsListPageComponent } from '../events-list-page/events-list-page.component';
import { EventResolver } from '../../../shared/resolvers/event.resolver';

const routes: Routes = [
  {
    path: ':id',
    component: EventPageComponent,
    resolve: {
      event: EventResolver,
    },
  },
  { path: '', component: EventsListPageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EventsShellRoutingModule {}
