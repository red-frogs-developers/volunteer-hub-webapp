import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminShellRoutingModule } from './admin-shell-routing.module';
import { AdminModule } from '../admin/admin.module';
import { AdminPage } from '../admin/admin.page';


@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    AdminShellRoutingModule,
  ]
})
export class AdminShellModule { }
