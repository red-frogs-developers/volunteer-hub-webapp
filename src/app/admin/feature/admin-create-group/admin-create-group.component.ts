import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GroupService } from '../../../shared/services/group.service';
import { GroupDocument } from '../../../shared/models/group.model';
import { CurrentUserService } from '../../../shared/services/current-user.service';
import { first } from 'rxjs';
import { User } from '../../../shared/models/user.model';
import { AngularFirestore } from '@angular/fire/compat/firestore';

@Component({
  selector: 'app-admin-create-group',
  templateUrl: './admin-create-group.component.html',
  styleUrls: ['./admin-create-group.component.scss'],
})
export class AdminCreateGroupComponent implements OnInit {
  createGroupForm!: FormGroup;
  createdGroup: GroupDocument | undefined;
  loading = false;
  success = false;

  constructor(
    private formBuilder: FormBuilder,
    private groupService: GroupService,
    private currentUserService: CurrentUserService,
    private angularFirestore: AngularFirestore
  ) {}

  ngOnInit(): void {
    this.createGroupForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      profile_picture: ['', [Validators.required]],
      description: ['', Validators.required],
      public: [true, Validators.required],
    });
  }

  get profilePicture() {
    return this.createGroupForm.get('profile_picture');
  }

  async submitForm() {
    this.loading = true;
    const formValue = this.createGroupForm.value;
    this.currentUserService.currentUser
      .pipe(first())
      .subscribe((user: User | null) => {
        if (user) {
          let newGroup: GroupDocument = new GroupDocument(
            [user.id],
            [],
            [user.id],
            [],
            formValue.name,
            formValue.profile_picture,
            formValue.description,
            formValue.public,
            this.angularFirestore.createId()
          );

          this.groupService.insertDocument(newGroup!).then(() => {
            console.log('Created group!');
            this.loading = false;
            this.createGroupForm.reset();
            this.success = true;
            this.groupService
              .getDocumentById(newGroup.id)
              .subscribe((group) => {
                this.createdGroup = group;
              });
          });
        }
      });
  }
}
