import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { regions } from '../../../shared/constants/regions';
import { UserDocument } from '../../../shared/models/user.model';
import * as firebase from 'firebase/app';
import { environment } from '../../../../environments/environment';
import {
  createUserWithEmailAndPassword,
  getAuth,
  sendPasswordResetEmail,
} from '@angular/fire/auth';
import { UserService } from '../../../shared/services/user.service';

@Component({
  selector: 'app-admin-register-user',
  templateUrl: './admin-register-user.component.html',
  styleUrls: ['./admin-register-user.component.scss'],
})
export class AdminRegisterUserComponent implements OnInit {
  secondaryApp = firebase.initializeApp(environment.firebase, 'Secondary');
  registerUserForm!: FormGroup;
  regions = regions;
  registeredUser: UserDocument | undefined;

  loading = false;
  success = false;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    const userRoles = this.formBuilder.group({
      admin: false,
      staff: false,
      volunteer: true,
    });

    this.registerUserForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      name: ['', [Validators.required]],
      phone_number: ['', [Validators.required]],
      region: ['', [Validators.required]],
      roles: userRoles,
      profile_picture: ['', [Validators.required]],
    });
  }

  async submitForm() {
    this.loading = true;
    const formValue = this.registerUserForm.value;
    let newUser: UserDocument;
    try {
      const auth = getAuth(this.secondaryApp);
      await createUserWithEmailAndPassword(
        auth,
        formValue.email,
        '123456'
      ).then((userCredential) => {
        newUser = new UserDocument(
          true,
          formValue.email,
          formValue.name,
          formValue.phone_number,
          formValue.profile_picture,
          formValue.region,
          formValue.roles,
          userCredential.user.uid
        );
        sendPasswordResetEmail(auth, formValue.email);
        auth.signOut();
      });
    } catch (e) {
      //  TODO
    } finally {
      await this.userService.insertDocument(newUser!).then(() => {
        console.log('inserted user');
        this.userService.getDocumentById(newUser.id).subscribe((user) => {
          this.registeredUser = user;
        });
      });
      this.loading = false;
      this.registerUserForm.reset();
      this.success = true;
    }
  }

  get profilePicture() {
    return this.registerUserForm.get('profile_picture');
  }
}
