import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminPage } from './admin.page';
import { AdminRegisterUserComponent } from '../admin-register-user/admin-register-user.component';
import { AdminCreateGroupComponent } from '../admin-create-group/admin-create-group.component';

const routes: Routes = [
  { path: '', component: AdminPage },
  {
    path: 'register',
    component: AdminRegisterUserComponent,
  },
  {
    path: 'create-group',
    component: AdminCreateGroupComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {}
