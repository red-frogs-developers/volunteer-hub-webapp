import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { MainContentWrapperModule } from 'src/app/shared/main-content-wrapper/main-content-wrapper.module';
import { AdminPage } from './admin.page';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { AdminRegisterUserComponent } from '../admin-register-user/admin-register-user.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { AdminCreateGroupComponent } from '../admin-create-group/admin-create-group.component';

@NgModule({
  declarations: [AdminPage, AdminRegisterUserComponent, AdminCreateGroupComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    MainContentWrapperModule,
    MatButtonModule,
    MatIconModule,
    MatListModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
  ],
})
export class AdminModule {}
