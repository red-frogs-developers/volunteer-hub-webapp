import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { EventDocument } from '../../../shared/models/event.model';
import { AngularFirestore } from '@angular/fire/compat/firestore';

@Component({
  selector: 'app-group-create-event-dialog',
  templateUrl: './group-create-event-dialog.component.html',
  styleUrls: ['./group-create-event-dialog.component.scss'],
})
export class GroupCreateEventDialogComponent implements OnInit {
  newEventForm!: FormGroup;

  loading = false;
  success = false;

  constructor(
    private angularFirestore: AngularFirestore,
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<GroupCreateEventDialogComponent>
  ) {}

  ngOnInit(): void {
    this.newEventForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      description: ['', Validators.required],
      eventLocation: ['', Validators.required],
      meetupLocation: ['', Validators.required],
      startDate: [new Date(), [Validators.required]],
      startTime: ['', [Validators.required]],
      endDate: [new Date(), [Validators.required]],
      endTime: ['', [Validators.required]],
    });
  }

  submitForm() {
    let value = this.newEventForm.value;
    let newEvent: EventDocument = new EventDocument(
      this.angularFirestore.createId(),
      value.name,
      '',
      value.description,
      value.eventLocation,
      value.meetupLocation,
      Date.parse(value.startDate),
      value.startTime,
      Date.parse(value.endDate),
      value.endTime,
      '',
      {
        invited: [],
        going: [],
        notGoing: [],
        tentative: [],
        partial: [],
      }
    );

    this.dialogRef.close(newEvent);
  }
}
