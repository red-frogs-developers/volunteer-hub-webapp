import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { UserDocument } from '../../../shared/models/user.model';

@Component({
  selector: 'app-group-add-member-dialog',
  templateUrl: './group-add-member-dialog.component.html',
  styleUrls: ['./group-add-member-dialog.component.scss'],
})
export class GroupAddMemberDialogComponent implements OnInit {
  unselectedUsers: UserDocument[] = [];
  selectedUsers: UserDocument[] = [];
  constructor(
    public dialogRef: MatDialogRef<GroupAddMemberDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { users: UserDocument[] }
  ) {
    console.log(data.users);
    this.unselectedUsers = data.users;
  }

  ngOnInit(): void {}

  onSelectUser(user: UserDocument) {
    this.unselectedUsers = this.unselectedUsers.filter(
      (data) => data.id != user.id
    );
    this.selectedUsers.push(user);
  }

  onDeselectUser(user: UserDocument) {
    this.selectedUsers = this.selectedUsers.filter(
      (data) => data.id != user.id
    );
    this.unselectedUsers.push(user);
  }

  closeDialog() {
    this.dialogRef.close(this.selectedUsers);
  }
}
