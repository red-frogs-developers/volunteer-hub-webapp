import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GroupSingleViewComponent } from './group-single-view.component';
import { GroupTabsContainerModule } from '../group-tabs-container/group-tabs-container.module';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [GroupSingleViewComponent],
  imports: [CommonModule, GroupTabsContainerModule, MatButtonModule],
  exports: [GroupSingleViewComponent],
})
export class GroupSingleViewModule {}
