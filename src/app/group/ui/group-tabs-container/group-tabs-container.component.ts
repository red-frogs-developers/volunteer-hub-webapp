import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { GroupNewPostDialogComponent } from '../group-new-post-dialog/group-new-post-dialog.component';
import { PostService } from '../../../shared/services/post.service';
import { GroupService } from '../../../shared/services/group.service';
import { Post } from '../../../shared/models/post.model';
import { first, Observable, Subscription } from 'rxjs';
import { GroupDocument } from '../../../shared/models/group.model';
import { AbstractFirestoreSmartComponent } from '../../../../../../angular-springfire';
import { GroupAddMemberDialogComponent } from '../group-add-member-dialog/group-add-member-dialog.component';
import { UserService } from '../../../shared/services/user.service';
import { UserDocument } from '../../../shared/models/user.model';
import { GroupCreateEventDialogComponent } from '../group-create-event-dialog/group-create-event-dialog.component';
import { EventDocument } from '../../../shared/models/event.model';
import { EventService } from '../../../shared/services/event.service';
import { CurrentUserService } from '../../../shared/services/current-user.service';

@Component({
  selector: 'app-group-tabs-container',
  templateUrl: './group-tabs-container.component.html',
  styleUrls: ['./group-tabs-container.component.scss'],
})
export class GroupTabsContainerComponent
  extends AbstractFirestoreSmartComponent<GroupDocument>
  implements OnInit, OnDestroy
{
  content = '';
  groupPosts: Observable<Post[]> | null = null;
  groupEvents: Observable<EventDocument[]> | null = null;
  groupMembers: Observable<UserDocument[]> | null = null;
  dialogSubscription: Subscription | null = null;
  currentGroup: GroupDocument | null = null;
  usersInGroup: UserDocument[] = [];

  constructor(
    public dialog: MatDialog,
    public postService: PostService,
    public groupService: GroupService,
    public userService: UserService,
    public eventService: EventService,
    private currentUserService: CurrentUserService
  ) {
    super(groupService);
  }

  ngOnInit(): void {
    this.setupSubscriptionToActiveDocument();
    if (this.document) {
      this.groupPosts = this.postService.getPostsIn(this.document);
      this.groupEvents = this.eventService.getEventsInArray(
        this.document.events
      );
      this.groupMembers = this.userService.getUsersNotInArray(
        this.document.members
      );
    }
    if (this.document?.members) {
      this.dialogSubscription = this.userService
        .getUsersNotInArray(this.document?.members)
        .subscribe((users) => {
          this.usersInGroup = users;
        });
    }
  }

  ngOnDestroy() {
    this.destroySubscription();
    if (this.dialogSubscription) {
      this.dialogSubscription.unsubscribe();
    }
  }

  openNewPostDialog() {
    const dialogRef = this.dialog.open(GroupNewPostDialogComponent, {
      data: { title: 'Test Title' },
      height: 'max-content',
      maxHeight: '65vh',
      width: '50vw',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result === '' || result === undefined || !this.document) {
        console.log('empty content');
      } else {
        this.postService.createPostInGroup(result, this.document);
      }
    });
  }

  openAddMemberDialog() {
    let dialogRef: MatDialogRef<any>;
    dialogRef = this.dialog.open(GroupAddMemberDialogComponent, {
      data: { users: this.usersInGroup },
      maxHeight: 'max-content',
      width: 'max-content',
    });
    dialogRef.afterClosed().subscribe((result: UserDocument[]) => {
      if (result === undefined || result.length === 0 || !this.document) {
        console.log('empty content');
      } else {
        let members: string[] = [];
        result.forEach((member) => {
          members.push(member.id);
        });
        this.groupService.addMembers(members);
      }
    });
  }

  openNewEventDialog() {
    const dialogRef = this.dialog.open(GroupCreateEventDialogComponent, {
      data: { title: 'Test Title' },
      height: 'max-content',
      maxHeight: 'min-content',
      width: '50vw',
    });

    dialogRef.afterClosed().subscribe((event: EventDocument) => {
      if (event === undefined || !this.document) {
        console.log('empty content');
      } else {
        this.currentUserService.currentUser
          .pipe(first())
          .subscribe((currentUser) => {
            if (currentUser && this.document) {
              console.log('making event');
              event.organizer = currentUser.id;
              event.responses.invited = this.document.members;
              event.group = this.document.id;
              this.eventService.createEventInGroup(event);
            }
          });
      }
    });
  }
}
