import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GroupTabsContainerComponent } from './group-tabs-container.component';
import { MatTabsModule } from '@angular/material/tabs';
import { GroupTabEventsViewModule } from '../group-tab-event-view/group-tab-events-view.module';
import { GroupTabPostsViewModule } from '../group-tab-posts-view/group-tab-posts-view.module';
import { GroupTabMembersViewModule } from '../group-tab-members-view/group-tab-members-view.module';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { QuillModule } from 'ngx-quill';

@NgModule({
  declarations: [GroupTabsContainerComponent],
  imports: [
    CommonModule,
    MatTabsModule,
    GroupTabEventsViewModule,
    GroupTabPostsViewModule,
    GroupTabMembersViewModule,
    MatButtonModule,
    MatDialogModule,
    QuillModule,
  ],
  exports: [GroupTabsContainerComponent],
})
export class GroupTabsContainerModule {}
