import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-group-new-post-dialog',
  templateUrl: './group-new-post-dialog.component.html',
  styleUrls: ['./group-new-post-dialog.component.scss'],
})
export class GroupNewPostDialogComponent implements OnInit {
  content = new FormControl('');

  constructor(
    public dialogRef: MatDialogRef<GroupNewPostDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { title: string }
  ) {}

  ngOnInit(): void {}

  closeDialog() {
    this.dialogRef.close(this.content.value);
  }

  onInput(event: any) {
    console.log(event.html);
  }
}
