import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GroupNewPostDialogComponent } from './group-new-post-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { ReactiveFormsModule } from '@angular/forms';
import { QuillModule } from 'ngx-quill';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [GroupNewPostDialogComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    ReactiveFormsModule,
    QuillModule,
    MatButtonModule,
  ],
  exports: [GroupNewPostDialogComponent],
})
export class GroupNewPostDialogModule {}
