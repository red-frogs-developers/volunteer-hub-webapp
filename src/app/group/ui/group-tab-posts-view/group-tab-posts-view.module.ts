import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GroupTabPostsViewComponent } from './group-tab-posts-view.component';
import { PostFeedViewModule } from '../../../shared/posts/post-feed-view/post-feed-view.module';

@NgModule({
  declarations: [GroupTabPostsViewComponent],
  imports: [CommonModule, PostFeedViewModule],
  exports: [GroupTabPostsViewComponent],
})
export class GroupTabPostsViewModule {}
