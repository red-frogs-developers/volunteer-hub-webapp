import { Component, Input, OnInit } from '@angular/core';
import { Post } from '../../../shared/models/post.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-group-tab-posts-view',
  templateUrl: './group-tab-posts-view.component.html',
  styleUrls: ['./group-tab-posts-view.component.scss'],
})
export class GroupTabPostsViewComponent implements OnInit {
  @Input()
  posts: Observable<Post[]> | null = null;

  constructor() {}

  ngOnInit(): void {}
}
