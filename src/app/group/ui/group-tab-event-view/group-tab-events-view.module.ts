import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GroupTabEventsViewComponent } from './group-tab-events-view.component';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { EventListViewModule } from '../../../shared/events/event-list-view/event-list-view.module';

@NgModule({
  declarations: [GroupTabEventsViewComponent],
  imports: [
    CommonModule,
    MatListModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    EventListViewModule,
  ],
  exports: [GroupTabEventsViewComponent],
})
export class GroupTabEventsViewModule {}
