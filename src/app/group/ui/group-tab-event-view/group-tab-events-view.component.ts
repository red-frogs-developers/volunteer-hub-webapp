import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { EventDocument } from '../../../shared/models/event.model';
import { GroupDocument } from '../../../shared/models/group.model';
import { AbstractFirestoreSmartComponent } from '../../../../../../angular-springfire';
import { GroupService } from '../../../shared/services/group.service';
import { UserService } from '../../../shared/services/user.service';
import { EventService } from '../../../shared/services/event.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-group-tab-events-view',
  templateUrl: './group-tab-events-view.component.html',
  styleUrls: ['./group-tab-events-view.component.scss'],
})
export class GroupTabEventsViewComponent
  extends AbstractFirestoreSmartComponent<GroupDocument>
  implements OnInit, OnDestroy
{
  @Input()
  events: Observable<EventDocument[]> | null = null;

  groupEvents!: EventDocument[];

  constructor(
    private groupService: GroupService,
    private userService: UserService,
    private eventService: EventService
  ) {
    super(groupService);
  }

  ngOnInit(): void {
    this.setupSubscriptionToActiveDocument();
    this.groupService.getActiveDocumentSubscription().subscribe((group) => {
      if (group) {
        let eventList = group.events;
        if (eventList) {
          this.eventService.getEventsInArray(eventList).subscribe((events) => {
            this.groupEvents = events;
          });
        }
      }
    });
  }

  ngOnDestroy(): void {
    this.destroySubscription();
  }
}
