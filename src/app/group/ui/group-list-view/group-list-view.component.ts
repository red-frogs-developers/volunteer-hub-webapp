import { Component, Input, OnInit } from '@angular/core';
import { GroupDocument } from '../../../shared/models/group.model';
import { User } from '../../../shared/models/user.model';
import { ActivatedRoute, Router } from '@angular/router';
import { GroupService } from '../../../shared/services/group.service';
import { CurrentUserService } from '../../../shared/services/current-user.service';

@Component({
  selector: 'app-group-list-view',
  templateUrl: './group-list-view.component.html',
  styleUrls: ['./group-list-view.component.scss'],
})
export class GroupListViewComponent implements OnInit {
  groups: GroupDocument[] | null = [];

  @Input()
  currentUser: User | null = null;

  constructor(
    private router: Router,
    private groupService: GroupService,
    private route: ActivatedRoute,
    private currentUserService: CurrentUserService
  ) {}

  ngOnInit(): void {
    this.currentUserService.currentUser.subscribe((user) => {
      this.groupService.getGroupsUserIsMember(user!.id).subscribe((groups) => {
        this.groups = groups;
      });
    });
  }

  selectGroup(group: GroupDocument) {
    this.router.navigate([group.id], { relativeTo: this.route }).then(() => {
      this.groupService.setActiveDocument(group);
    });
  }
}
