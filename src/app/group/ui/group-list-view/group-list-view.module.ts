import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GroupListViewComponent } from './group-list-view.component';
import { MatListModule } from '@angular/material/list';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [GroupListViewComponent],
  imports: [CommonModule, MatListModule, RouterModule],
  exports: [GroupListViewComponent],
})
export class GroupListViewModule {}
