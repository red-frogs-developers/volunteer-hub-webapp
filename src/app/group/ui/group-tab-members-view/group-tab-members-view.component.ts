import { Component, OnDestroy, OnInit } from '@angular/core';
import { GroupService } from '../../../shared/services/group.service';
import { UserService } from '../../../shared/services/user.service';
import { GroupDocument } from '../../../shared/models/group.model';
import { AbstractFirestoreSmartComponent } from '../../../../../../angular-springfire';
import { UserDocument } from '../../../shared/models/user.model';

@Component({
  selector: 'app-group-tab-members-view',
  templateUrl: './group-tab-members-view.component.html',
  styleUrls: ['./group-tab-members-view.component.scss'],
})
export class GroupTabMembersViewComponent
  extends AbstractFirestoreSmartComponent<GroupDocument>
  implements OnInit, OnDestroy
{
  groupMembers!: UserDocument[];
  groupAdmins: UserDocument[] = [];

  constructor(
    private groupService: GroupService,
    private userService: UserService
  ) {
    super(groupService);
  }

  ngOnInit(): void {
    this.setupSubscriptionToActiveDocument();
    let membersList = this.document?.members;
    let adminList = this.document?.admins;
    if (membersList) {
      this.userService
        .getUsersInArray(membersList)

        .subscribe((members) => {
          let admins: UserDocument[] = [];
          this.groupMembers = members.filter((user) => {
            if (adminList?.includes(user.id)) {
              admins.push(user);
              return false;
            } else {
              return true;
            }
          });
          this.groupAdmins = admins;
        });
    }
  }

  ngOnDestroy(): void {
    this.destroySubscription();
  }
}
