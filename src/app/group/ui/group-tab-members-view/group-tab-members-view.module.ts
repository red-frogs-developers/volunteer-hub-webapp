import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GroupTabMembersViewComponent } from './group-tab-members-view.component';
import { MatListModule } from '@angular/material/list';
import { RouterModule } from '@angular/router';
import { UserProfileCardModule } from '../../../shared/user-profile-card/user-profile-card.module';

@NgModule({
  declarations: [GroupTabMembersViewComponent],
  imports: [CommonModule, MatListModule, RouterModule, UserProfileCardModule],
  exports: [GroupTabMembersViewComponent],
})
export class GroupTabMembersViewModule {}
