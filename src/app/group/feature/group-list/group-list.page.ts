import { Component, OnDestroy, OnInit } from '@angular/core';
import { GroupDocument } from '../../../shared/models/group.model';
import { Subscription } from 'rxjs';
import { CurrentUserService } from '../../../shared/services/current-user.service';
import { User } from '../../../shared/models/user.model';
import { GroupService } from '../../../shared/services/group.service';

@Component({
  selector: 'app-group-list',
  templateUrl: './group-list.page.html',
  styleUrls: ['./group-list.page.scss'],
})
export class GroupListPage implements OnInit, OnDestroy {
  currentUserSubscription: Subscription | null = null;
  groupsSubscription: Subscription | null = null;

  groups: GroupDocument[] = [];
  currentUser!: User | null;

  constructor(
    public currentUserService: CurrentUserService,
    public groupService: GroupService
  ) {}

  ngOnInit(): void {
    this.currentUserSubscription =
      this.currentUserService.currentUser.subscribe((currentUser) => {
        this.currentUser = currentUser;
      });
    this.groupService.getGroups();
    this.groupsSubscription = this.groupService.groups.subscribe((groups) => {
      this.groups = groups;
    });
  }

  ngOnDestroy(): void {
    this.currentUserSubscription?.unsubscribe();
    this.groupsSubscription?.unsubscribe();
  }
}
