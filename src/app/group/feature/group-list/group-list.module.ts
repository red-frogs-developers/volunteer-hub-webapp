import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GroupListRoutingModule } from './group-list-routing.module';
import { GroupListPage } from './group-list.page';
import { GroupListViewModule } from '../../ui/group-list-view/group-list-view.module';
import { MainContentWrapperModule } from '../../../shared/main-content-wrapper/main-content-wrapper.module';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';

@NgModule({
  declarations: [GroupListPage],
  imports: [
    CommonModule,
    GroupListRoutingModule,
    GroupListViewModule,
    MainContentWrapperModule,
    MatCardModule,
    MatButtonModule,
    MatListModule,
  ],
})
export class GroupListModule {}
