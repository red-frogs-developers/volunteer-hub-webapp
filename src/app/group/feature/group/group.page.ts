import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { GroupDocument } from '../../../shared/models/group.model';
import { GroupService } from '../../../shared/services/group.service';
import { AbstractFirestoreSmartComponent } from '../../../../../../angular-springfire';

@Component({
  selector: 'app-group',
  templateUrl: './group.page.html',
  styleUrls: ['./group.page.scss'],
})
export class GroupPage
  extends AbstractFirestoreSmartComponent<GroupDocument>
  implements OnInit, OnDestroy
{
  groupId!: string;

  constructor(
    private route: ActivatedRoute,
    private angularFirestore: AngularFirestore,
    private groupService: GroupService,
    private activatedRoute: ActivatedRoute
  ) {
    super(groupService);
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ group }) => {
      this.setupSubscriptionWithDocument(group);
    });
  }

  ngOnDestroy(): void {
    this.destroySubscription();
  }
}
