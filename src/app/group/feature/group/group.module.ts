import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GroupRoutingModule } from './group-routing.module';
import { GroupPage } from './group.page';
import { MainContentWrapperModule } from '../../../shared/main-content-wrapper/main-content-wrapper.module';
import { GroupSingleViewModule } from '../../ui/group-single-view/group-single-view.module';
import { GroupCreateEventDialogComponent } from '../../ui/group-create-event-dialog/group-create-event-dialog.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [GroupPage, GroupCreateEventDialogComponent],
  imports: [
    CommonModule,
    GroupRoutingModule,
    MainContentWrapperModule,
    GroupSingleViewModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatButtonModule,
  ],
})
export class GroupModule {}
