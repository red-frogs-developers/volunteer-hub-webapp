import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GroupShellRoutingModule } from './group-shell-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, GroupShellRoutingModule],
})
export class GroupShellModule {}
