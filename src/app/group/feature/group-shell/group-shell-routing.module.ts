import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {GroupResolver} from "../../../shared/resolvers/group.resolver";

const routes: Routes = [
  {
    path: ':id',
    loadChildren: () =>
      import('../group/group.module').then((m) => m.GroupModule),
    resolve: {
      group: GroupResolver
    }
  },
  {
    path: '',
    loadChildren: () =>
      import('../group-list/group-list.module').then((m) => m.GroupListModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GroupShellRoutingModule {}
