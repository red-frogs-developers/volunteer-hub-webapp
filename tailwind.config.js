module.exports = {
  content: [
    "./src/**/*.{html,ts}",
  ],
  theme: {
    extend: {
    colors: {
      'primary': '#ff294d',
      'accent': '#22252a',
    },
    },
  },
  plugins: [],
}
